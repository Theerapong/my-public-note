https://docs.gitlab.com/ee/user/project/clusters/add_existing_cluster.html


apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: gitlab
    namespace: kube-system

# kubectl apply -f gitlab-admin-service-account.yaml


# retrive ออกมา
# kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab | awk '{print $1}')





******* API URL *******
➜  learn-terraform-provision-eks-cluster git:(main) ✗ kubectl cluster-info | grep -E 'Kubernetes master|Kubernetes control plane' | awk '/http/ {print $NF}'
https://77F2D4E9CF41077914F4F6023ACF608A.gr7.eu-west-1.eks.amazonaws.com






****** CA Certificate ******

➜  learn-terraform-provision-eks-cluster git:(main) ✗
➜  learn-terraform-provision-eks-cluster git:(main) ✗ kubectl get secrets
NAME                  TYPE                                  DATA   AGE
default-token-zvlz2   kubernetes.io/service-account-token   3      38m
➜  learn-terraform-provision-eks-cluster git:(main) ✗
➜  learn-terraform-provision-eks-cluster git:(main) ✗
➜  learn-terraform-provision-eks-cluster git:(main) ✗
➜  learn-terraform-provision-eks-cluster git:(main) ✗ kubectl get secret default-token-zvlz2 -o jsonpath="{['data']['ca\.crt']}" | base64 --decode
-----BEGIN CERTIFICATE-----
MIIC/jCCAeagAwIBAgIBADANBgkqhkiG9w0BAQsFADAVMRMwEQYDVQQDEwprdWJl
